# Netax bordrosuna alacağımız 2-3 yıl deneyimli android developer


Android (Kotlin, Java) konusunda uzman,

En az 2 yıllık deneyime sahip,

REST API, JSON, XML, Web servisler hakkında bilgi sahibi,

Kod kalitesine ve dokümantasyonuna önem veren,

Ekip çalışmasına uyumlu, yeni teknolojileri öğrenmeye meraklı ve araştırmacı,

Erkek adaylar için askerlik hizmetini tamamlamış veya en az 2 yıl tecilli adaylar aranmaktadır


## Uzmanlık

- Android (Kotlin, Java) konusunda uzman,
- REST API, JSON, XML, Web servisler hakkında bilgi sahibi,


## Olsa iyi olur

- Kod kalitesine ve dokümantasyonuna önem veren,
- Ekip çalışmasına uyumlu, yeni teknolojileri öğrenmeye meraklı ve araştırmacı,

## Kısıtlar

- Erkek adaylar için askerlik hizmetini tamamlamış veya en az 2 yıl tecilli adaylar aranmaktadır
